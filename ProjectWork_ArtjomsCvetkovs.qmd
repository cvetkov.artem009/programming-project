---
title: "Movie Dataset Analysis"
author: "Artjoms Cvetkovs"
date: "2024-06-07"
format: html
---

Introduction

In this project, we aim to analyze a movie dataset from Kaggle. The dataset contains various attributes related to movies such as their ratings, genres, release years, scores, votes, directors, and revenues. Our goal is to explore and analyze the data to gain insights into trends and patterns within the movie industry.

Presentation of the Data

We begin by loading the necessary libraries and reading the dataset. Then, we will examine the first few rows and the structure of the dataset to understand its contents.

```{r}
# I set the CRAN because Rstudio during rendering wrote about the mistake that he was not
options(repos = c(CRAN = "https://cran.rstudio.com"))
install.packages("tidyverse")
library(tidyverse)
movie_data <- read_csv("movies.csv")

head(movie_data)
str(movie_data)
```

Genre Summary: The average rating, total number of movies, and total revenue for each genre. Year Summary: The average budget, average gross revenue, and total number of movies released each year. Director Summary: The average rating and total number of movies for the top 10 directors with the most movies.

The dataset contains the following columns:

name: The title of the movie.

rating: The movie's rating (e.g., PG, R).

genre: Genre of the movie (e.g., Drama, Comedy).

year: Year of release.

released: Release date.

score: IMDB score.

votes: Number of votes on IMDB.

director: Director of the movie.

writer: Writer of the movie.

star: Leading star of the movie.

country: Country of origin.

budget: Budget of the movie (in USD).

gross: Gross revenue (in USD).

company: Production company.

runtime: Runtime of the movie (in minutes).

## Aggregation of data

To better understand the dataset, we will perform some aggregations. We will aggregate the data by genre, year, and director to uncover interesting insights.

```{r}
# Aggregating data by genre
genre_summary <- movie_data %>%
  group_by(genre) %>%
  summarise(
    average_rating = mean(score, na.rm = TRUE),
    total_movies = n(),
    total_revenue = sum(gross, na.rm = TRUE)
  )
# Aggregating data by year
year_summary <- movie_data %>%
  group_by(year) %>%
  summarise(
    average_budget = mean(budget, na.rm = TRUE),
    average_gross = mean(gross, na.rm = TRUE),
    total_movies = n()
  )

# Aggregating data by director
director_summary <- movie_data %>%
  group_by(director) %>%
  summarise(
    average_rating = mean(score, na.rm = TRUE),
    total_movies = n()
  ) %>%
  arrange(desc(total_movies)) %>%
  head(10)
genre_summary
year_summary
director_summary
```

### Exploratory Data Analysis

Next, we will create some visualizations to explore the data further. We will use scatter plots, box plots, and histograms to reveal trends and patterns.

#### Revenue vs Runtime

We will start by examining the relationship between a movie's runtime and its revenue.

```{r}
cleaned_movie_data <- movie_data %>%
  filter(!is.na(runtime), !is.na(gross))
ggplot(cleaned_movie_data, aes(x = runtime, y = gross)) +
  geom_point(na.rm = TRUE) +
  labs(title = "Revenue vs Runtime",
       x = "Runtime (minutes)",
       y = "Gross Revenue (USD)")
```

In this scatter plot, each point represents a movie. The x-axis shows the runtime in minutes, and the y-axis shows the gross revenue in USD. This visualization helps us understand if there is any correlation between the runtime of a movie and its revenue.

#### Rating Distribution by Genre

Next, we will look at the distribution of IMDB ratings across different genres.

```{r}
cleaned_movie_data <- movie_data %>%
  filter(!is.na(genre), !is.na(score))

ggplot(cleaned_movie_data, aes(x = genre, y = score)) +
  geom_boxplot() +
  labs(title = "Rating Distribution by Genre",
       x = "Genre",
       y = "IMDB Score")
```

This box plot displays the distribution of IMDB scores for each genre. The boxes show the interquartile range, and the lines extending from the boxes show the range of scores. This plot helps us understand which genres tend to have higher or lower ratings.

#### Distribution of Movie Ratings

Finally, we will create a histogram to visualize the distribution of movie ratings.

```{r}
cleaned_movie_data <- movie_data %>%
  filter(!is.na(score))

ggplot(cleaned_movie_data, aes(x = score)) +
  geom_histogram(binwidth = 0.5, fill = "blue", color = "black") +
  labs(title = "Distribution of Movie Ratings",
       x = "IMDB Score",
       y = "Frequency")
```

This histogram shows the frequency of different IMDB scores. It helps us see how movie ratings are distributed and identify any common rating ranges.

### Conclusion

Our analysis of the movie dataset reveals several interesting insights:

-   The average ratings, number of movies, and total revenue vary significantly across different genres.

-   Over the years, the average budget and gross revenue of movies have shown notable trends.

-   The top directors with the most movies have varying average ratings.

-   There is no clear correlation between a movie's runtime and its revenue.

-   Certain genres tend to have higher or lower ratings, as seen in the rating distribution by genre.

-   The distribution of movie ratings shows that most movies have scores concentrated around certain values.

By exploring and visualizing the data, we have gained a deeper understanding of trends and patterns within the movie industry.

# References

[Kaggle dataset: Movies Dataset](https://www.kaggle.com/datasets/danielgrijalvas/movies)
